PROJECT
-------
An endpoint application to open saving accounts.

LANGUAGE & FRAMEWORKS
---------------------
Java : jdk1.8.0_191
Maven : 3.6.1
Spring-boot : 2.1.5

INSTALLING
----------
The application is an executable .jar file. 
To execute the application on windows, go to jar folder from command prompt and execute the command:
java -jar $jarname.jar

HOW TO USE
----------
There is no UI for the applciation.
After starting the jar file, application can be tested through a web browser.
The main address for the application is http://localhost:8080/accounts

There are three parameters to open a saving account:
0- id : user id, Long, mandatory
1- accountName : name for the saving account, String, optional
2- initialAmount : first balance amount of the account, Double, optional.

sample usage:
- http://localhost:8080/accounts?id=5
- http://localhost:8080/accounts?id=5&accountName=myAccount
- http://localhost:8080/hello?id=5&accountName=myAccount&initialAmount=120

The accounts saved through this application is holded in memory.